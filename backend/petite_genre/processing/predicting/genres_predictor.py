from typing import List

from genre_recognition import MODEL_PATH
from genre_recognition.common import GENRES
from genre_recognition.genre_recognizer import GenreRecognizer
from logger import logger
from petite_genre.core.track import Track
from petite_genre.core.prediction import Prediction


class GenresPredictor(object):
    def __init__(self, tracks: List[Track]):
        self._tracks = tracks
        self._genre_recognizer = GenreRecognizer(MODEL_PATH)
        self.predictions = []

    def predict_all(self):
        for track in self._tracks:
            try:
                self.predictions.append(self._get_prediction_for_track(track))
            except Exception as e:
                logger.warning(
                    f"Could not predict genre for track: {track}. Error: {e}"
                )
                self.predictions.append(Prediction(track=track, successful=False))

    def _get_prediction_for_track(self, track):
        predictions, duration, before_softmax = self._genre_recognizer.recognize(
            track.track_path
        )
        distribution = self._genre_recognizer.get_genre_distribution_over_time(
            predictions, duration
        )
        return Prediction(
            track=track,
            predicted_genre=self._get_prediction_from_mean_distribution(distribution),
            successful=True,
            distribution=distribution,
            before_softmax_activations=before_softmax,
        )

    def _get_prediction_from_mean_distribution(self, distribution):
        mean_distribution = {genre: 0.0 for genre in GENRES}
        for _, distrib in distribution:
            for genre in GENRES:
                mean_distribution[genre] += distrib[genre]

        for genre in GENRES:
            mean_distribution[genre] /= len(distribution)

        return max(mean_distribution, key=mean_distribution.get)
