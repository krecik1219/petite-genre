from flask import request
from flask_restful import Resource, reqparse

import petite_genre.database.db_types as db
from logger import logger
from petite_genre.constants import (
    HTTP_SUCCESS_RESPONSE_CODE,
    HTTP_UNPROCESSABLE_ENTITY_CODE,
    HTTP_INTERNAL_SERVER_ERROR_CODE,
    HTTP_NOT_FOUND_CODE,
)
from petite_genre.resources.helpers.processing_result_helper import (
    ProcessingResultHelper,
)
from petite_genre.utils.response_utils import make_compressed_response


class SessionRecord(Resource):
    parser = reqparse.RequestParser(bundle_errors=True)
    parser.add_argument("session_id", type=str, required=True)

    def get(self):
        logger.info(f"Processing session record request: {request.args['session_id']}")
        data = self.parser.parse_args(http_error_code=HTTP_UNPROCESSABLE_ENTITY_CODE)
        try:
            return self._build_response(data)
        except Exception as e:
            logger.error(f"Error during processing result req: {e}")
            return {"message": "Internal server error"}, HTTP_INTERNAL_SERVER_ERROR_CODE

    def _build_response(self, data):
        session_record = db.SessionRecord.objects(session_id=data["session_id"]).first()
        if session_record is None:
            return {
                "message": f"Session record with given session_id: {data['session_id']} does not exist"
            }, HTTP_NOT_FOUND_CODE

        training_processing_result = ProcessingResultHelper.get_processing_result(
            session_record.training_result
        )

        additional_results = [
            ProcessingResultHelper.get_processing_result(result)
            for result in session_record.additional_results
        ]

        result_json = {
            "session_id": session_record.session_id,
            "training_result": training_processing_result,
            "additional_results": additional_results,
        }

        return make_compressed_response(result_json, HTTP_SUCCESS_RESPONSE_CODE)
