import os
from typing import Dict

from mongoengine import (
    EmbeddedDocument,
    StringField,
    EmbeddedDocumentListField,
    IntField,
)

from petite_genre.database.db_types.track import Track
from petite_genre.database.db_types.track_dim_reduced import TrackDimReduced
import petite_genre.core as core


class DimReduction(EmbeddedDocument):
    method_used = StringField(required=True)
    reduction_model_path = StringField(required=True)
    new_dims_num = IntField(required=True)
    tracks_dim_reductions = EmbeddedDocumentListField(TrackDimReduced, required=True)

    @staticmethod
    def make(
        path_to_user_data_dir: str,
        dim_reduction_result: core.DimReductionResult,
        tracks: Dict[str, Track],
    ):
        reduction_model_path = os.path.join(path_to_user_data_dir, "reduction_model")
        dim_reduction_result.dim_reduction.save(reduction_model_path)
        tracks_reductions = [
            TrackDimReduced(
                track=tracks[reduction.track.uuid],
                reduced_dim=reduction.dim_reduced_output,
            )
            for reduction in dim_reduction_result.tracks_with_reduced_dim
        ]

        return DimReduction(
            method_used=dim_reduction_result.method_used.name,
            reduction_model_path=reduction_model_path,
            new_dims_num=dim_reduction_result.new_dims_num,
            tracks_dim_reductions=tracks_reductions,
        )
