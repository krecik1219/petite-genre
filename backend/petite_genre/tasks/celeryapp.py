import os
import shutil

from celery import Celery

from petite_genre.constants import (
    CELERY_BROKER_URL,
    CELERY_RESULT_BACKEND,
    UPLOAD_DIR,
)
from logger import logger, logger_init
from petite_genre.database.db_setup import db_init


def celery_init():
    logger_init("celery")
    logger.info("Celery setup")
    if os.getenv("DOCKER"):
        logger.debug("Running in docker environment.")
        shutil.chown(UPLOAD_DIR, "celeryuser", "celeryuser")
    db_init()


def make_celery():
    celery = Celery(__name__, backend=CELERY_RESULT_BACKEND, broker=CELERY_BROKER_URL)
    celery_init()
    return celery


celery = make_celery()
