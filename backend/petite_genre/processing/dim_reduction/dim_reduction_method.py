from enum import IntEnum


DEFAULT_RESULT_DIMS_NUM = 3


class DimReductionMethod(IntEnum):
    PCA = 0
