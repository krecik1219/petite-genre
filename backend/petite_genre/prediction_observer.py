import numpy as np
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
from sklearn.preprocessing import normalize

from common import Prediction
from common import GENRES_TO_COLORS


class PredictionObserver:
    def __init__(self):
        self._figure = None
        self._ax = None
        self._genres_values = {}

    def __enter__(self):
        self._figure = plt.figure()
        self._ax = self._figure.add_subplot(111, projection="3d")
        return self

    def __exit__(self, *args):
        output, indices_for_genres = self._accumulate_outputs()

        normalized_output = normalize(output, norm="l2")
        whitening_pca = PCA(n_components=3, whiten=True)
        whitened_output = whitening_pca.fit_transform(normalized_output)
        normalized_whitened_output = normalize(whitened_output, norm="l2")

        for genre in self._genres_values:
            begin, end = indices_for_genres[genre]
            self._ax.scatter(
                normalized_whitened_output[begin:end, 0],
                normalized_whitened_output[begin:end, 1],
                normalized_whitened_output[begin:end, 2],
                label=genre,
                c=GENRES_TO_COLORS[genre],
            )

        self._ax.legend()
        plt.show()

    def notify(self, prediction: Prediction):
        if prediction.predicted_genre in self._genres_values:
            self._genres_values[prediction.predicted_genre] = np.append(
                self._genres_values[prediction.predicted_genre],
                prediction.before_softmax,
                axis=0,
            )
        else:
            self._genres_values[prediction.predicted_genre] = prediction.before_softmax

    def _accumulate_outputs(self):
        total = None
        genres_indices = {}
        for key, arr in self._genres_values.items():
            if total is None:
                total = arr
                genres_indices[key] = (0, arr.shape[0])
            else:
                genres_indices[key] = (total.shape[0], total.shape[0] + arr.shape[0])
                total = np.append(total, arr, axis=0)

        return total, genres_indices
