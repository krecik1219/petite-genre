import pickle
from typing import List

from sklearn.decomposition import PCA
from sklearn.preprocessing import normalize

import numpy as np

from petite_genre.processing.dim_reduction.abstract_dim_reduction import (
    AbstractDimReduction,
    DimReductionError,
)

from petite_genre.core.dim_reduction_result import TrackWithReducedDim
from petite_genre.core.prediction import Prediction
from petite_genre.processing.dim_reduction.dim_reduction_method import (
    DimReductionMethod,
)


class PcaWrapper(AbstractDimReduction):
    PICKLE_PROTOCOL_VERSION = 4

    def __init__(self, result_dims_num):
        super().__init__()
        self._result_dims_num = result_dims_num
        self._pca = None

    def _get_reduction_method(self):
        return DimReductionMethod.PCA

    def _get_new_dims_num(self):
        return self._result_dims_num

    def save(self, path):
        with open(path, "wb") as f:
            pickle.dump(self._pca, f, protocol=PcaWrapper.PICKLE_PROTOCOL_VERSION)

    def load(self, path):
        with open(path, "rb") as f:
            self._pca = pickle.load(f)

    def _reduce_predictions_dimensions(self, predictions: List[Prediction]):
        track_to_output_mapping, concatenated_output = self._concatenate_outputs(
            predictions
        )
        normalized_output = normalize(concatenated_output, norm="l2")
        self._fit_pca_if_needed(normalized_output)
        transformed_output = self._pca.transform(normalized_output)
        normalized_transformed_output = normalize(transformed_output, norm="l2")
        tracks_with_reduced_dim = []
        for track, row in track_to_output_mapping.items():
            tracks_with_reduced_dim.append(
                TrackWithReducedDim(
                    track=track,
                    dim_reduced_output=normalized_transformed_output[row, :],
                )
            )

        return tracks_with_reduced_dim

    def _concatenate_outputs(self, predictions):
        predictions_iter = iter(predictions)
        prediction = next(predictions_iter)
        while not prediction.successful:
            prediction = next(predictions_iter)

        if not prediction.successful:
            raise DimReductionError(
                "No single successful prediction passed to dim reduction"
            )

        concatenated_output = prediction.before_softmax_activations
        track_to_output_mapping = {prediction.track: 0}
        row_num = 1
        for prediction in predictions_iter:
            if prediction.successful:
                concatenated_output = np.vstack(
                    (concatenated_output, prediction.before_softmax_activations)
                )
                track_to_output_mapping[prediction.track] = row_num
                row_num += 1

        return track_to_output_mapping, concatenated_output

    def _fit_pca_if_needed(self, X):
        if self._pca is None:
            self._pca = PCA(n_components=self._result_dims_num, whiten=True)
            self._pca.fit(X)
