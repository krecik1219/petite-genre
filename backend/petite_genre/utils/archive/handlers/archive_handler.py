from abc import ABC, abstractmethod


class ArchiveHandler(ABC):
    def __init__(self, path_to_archive=None, file_obj=None):
        self._path_to_archive = path_to_archive
        self._file_obj = file_obj

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()

    @abstractmethod
    def close(self):
        pass

    @abstractmethod
    def extract_all(self, output_dir_path):
        pass
