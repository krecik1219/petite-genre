class ProcessingResultHelper(object):
    @staticmethod
    def get_processing_result(processing_result):
        tracks = ProcessingResultHelper._get_tracks(processing_result)
        predictions = ProcessingResultHelper._get_predictions(processing_result)
        dim_reductions = ProcessingResultHelper._get_dim_reductions(processing_result)
        return {
            "uuid": processing_result.uuid,
            "date": processing_result.date,
            "tracks": tracks,
            "predictions": predictions,
            "dim_reduction": dim_reductions,
        }

    @staticmethod
    def _get_tracks(processing_result):
        return {
            track.uuid: track.to_mongo()
            for track in processing_result.tracks_data.tracks
        }

    @staticmethod
    def _get_predictions(processing_result):
        return {
            prediction.track.uuid: {
                "predicted_genre": prediction.predicted_genre,
                "distribution": prediction.distribution,
            }
            for prediction in processing_result.predictions
        }

    @staticmethod
    def _get_dim_reductions(processing_result):
        return {
            track_dim_reduced.track.uuid: track_dim_reduced.reduced_dim
            for track_dim_reduced in processing_result.dim_reduction.tracks_dim_reductions
        }
