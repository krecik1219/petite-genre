## Genre-recognition
This project embeds code from [https://github.com/deepsound-project/genre-recognition](https://github.com/deepsound-project/genre-recognition)

## Prerequisites
- Python >= 3.5 && <= 3.7 as tensorflow 1.9.0 is required (tested on python3.6.9)
- node
- [yarn](https://classic.yarnpkg.com/en/docs/install) (or npm)
- docker-compose
- docker
- .env - file with environment variables for docker

## Git-lfs

First time after clone

```setup
git-lfs install && git-lfs pull
```

## Docker setup
User should have locally created .env file with ports assignments for PETITE_GENRE_BACKEND_PORT and PETITE_GENRE_FRONTEND_PORT.

Example of .env file is provided in repo

```shell
cp .env_example .env
```
**In order to enable e-mail sending feature, user should provide MAIL_ADDRESS and MAIL_PASSWORD in .env file.**

#### Start containers

```shell
cd petite-genre
source .env
sudo docker-compose up
```

### Rebuild images

Required to apply src code changes

```shell
sudo docker-compose up --build
```

## Standard setup

### Backend

#### Virtual env

```shell
cd backend
# assuming python points to python >= 3.5 && <= 3.7 
python -m venv venv
source venv/bin/activate
python -m pip install --upgrade pip
python -m pip install -r requirements.txt
```

#### Starting app
```shell
# assuming python virtualenv is active and cwd is backend
cd petite_genre
flask run
```

#### Backend logs
All backend-related logs can be found in **petite-genre/backend/logs** directory.


### Frontend

#### Install Frontend Requirements

```shell
cd frontend
yarn install
```

#### Start App

```shell
yarn start
```

## Development

#### frontend tests/linter:

```shell
cd frontend
yarn test
yarn lint
```

### backend code formatting
```shell
cd backend
# assuming python virtualenv is active
black .
```

#### backend tests/linter:
```shell
cd backend
# assuming python virtualenv is active
tox -e test
tox -e lint
```

#### Dataset
Download dataset from http://opihi.cs.uvic.ca/sound/genres.tar.gz
```shell
# using aria2
aria2c -x 16 -s 16 http://opihi.cs.uvic.ca/sound/genres.tar.gz
# extract
mkdir gtzan_dataset && tar -C ./gtzan_dataset -xvzf genres.tar.gz
```

#### Verify model on dataset
```shell
cd backend/petite_genre
# assuming venv is active
python verify_model.py <path to gtzan dataset genres, eg. /home/me/gtzan_dataset/genres/>
```
