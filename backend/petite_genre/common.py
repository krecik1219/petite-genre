import numpy as np

from dataclasses import dataclass


@dataclass
class Prediction:
    file: str = ""
    genre: str = ""
    predicted_genre: str = ""
    before_softmax: np.ndarray = None


# randomly picked from https://matplotlib.org/3.1.0/gallery/color/named_colors.html
GENRES_TO_COLORS = {
    "blues": "brown",
    "classical": "darkmagenta",
    "country": "dimgray",
    "disco": "red",
    "hiphop": "navy",
    "jazz": "seagreen",
    "metal": "black",
    "pop": "orange",
    "reggae": "deeppink",
    "rock": "teal",
}
