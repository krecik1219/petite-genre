import smtplib
from email.mime.text import MIMEText

from petite_genre.constants import MAIL_CREDENTIALS, WEBSITE_URL

SMTP_SERVER_DATA = {"address": "smtp.gmail.com", "port": 465}


class MailingException(Exception):
    pass


def construct_mail(subject, body):
    mail = MIMEText(body, "html")
    mail["Subject"] = subject

    return mail


def send_mail(receiver_address, subject, body):
    mail = construct_mail(subject, body)

    with smtplib.SMTP_SSL(
        SMTP_SERVER_DATA["address"], SMTP_SERVER_DATA["port"]
    ) as server:
        server.login(MAIL_CREDENTIALS["address"], MAIL_CREDENTIALS["password"])
        server.sendmail(MAIL_CREDENTIALS["address"], receiver_address, mail.as_string())


def send_analysis_result(session_id, uuid, mail_address):
    try:
        send_mail(
            receiver_address=mail_address,
            subject="petite-genre: Tracks processing result",
            body=f"Processing successful, session id: {session_id},"
            f' results url: <a href="{WEBSITE_URL}/result/{uuid}">{WEBSITE_URL}/result/{uuid}</a>',
        )
    except Exception as e:
        raise MailingException("Sending mail failed with msg: " + str(e))


def send_failed_analysis_result(session_id, mail_address):
    try:
        send_mail(
            receiver_address=mail_address,
            subject="petite-genre: Tracks processing failed",
            body=f"Analysis failed. Session id: {session_id}",
        )
    except Exception as e:
        raise MailingException("Sending mail failed with msg: " + str(e))
