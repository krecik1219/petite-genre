from typing import Dict

import numpy as np

from dataclasses import dataclass

from petite_genre.core.track import Track


@dataclass
class Prediction:
    track: Track = None
    predicted_genre: str = ""
    successful: bool = False
    distribution: Dict[float, Dict[str, float]] = None
    before_softmax_activations: np.ndarray = None
