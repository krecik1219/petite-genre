from flask import request
from flask_restful import Resource, reqparse

import petite_genre.database.db_types as db
from logger import logger
from petite_genre.constants import (
    HTTP_SUCCESS_RESPONSE_CODE,
    HTTP_UNPROCESSABLE_ENTITY_CODE,
    HTTP_INTERNAL_SERVER_ERROR_CODE,
    HTTP_NOT_FOUND_CODE,
)
from petite_genre.resources.helpers.processing_result_helper import (
    ProcessingResultHelper,
)
from petite_genre.utils.response_utils import make_compressed_response


class ProcessingResult(Resource):
    parser = reqparse.RequestParser(bundle_errors=True)
    parser.add_argument("uuid", type=str, required=True)

    def get(self):
        logger.info(f"Processing result request: {request.args['uuid']}")
        data = self.parser.parse_args(http_error_code=HTTP_UNPROCESSABLE_ENTITY_CODE)
        try:
            return self._build_response(data)
        except Exception as e:
            logger.error(f"Error during processing result req: {e}")
            return {"message": "Internal server error"}, HTTP_INTERNAL_SERVER_ERROR_CODE

    def _build_response(self, data):
        processing_result = db.ProcessingResult.objects(uuid=data["uuid"]).first()
        if processing_result is None:
            return {
                "message": f"Processing result with given uuid: {data['uuid']} does not exist"
            }, HTTP_NOT_FOUND_CODE

        result_json = ProcessingResultHelper.get_processing_result(processing_result)
        return make_compressed_response(result_json, HTTP_SUCCESS_RESPONSE_CODE)
