from mongoengine import Document, StringField, ReferenceField, ListField

from petite_genre.constants import MONGO_DB_ALIAS
from .processing_result import ProcessingResult


class SessionRecord(Document):
    session_id = StringField(primary_key=True)
    training_result = ReferenceField(ProcessingResult)
    additional_results = ListField(ReferenceField(ProcessingResult))

    meta = {"db_alias": MONGO_DB_ALIAS, "collection": "session_records"}
