import enum

from petite_genre.utils.archive.archive_error import ArchiveError
from petite_genre.utils.archive.handlers.tar_handler import TarHandler
from petite_genre.utils.archive.handlers.zip_handler import ZipHandler
from petite_genre.utils.path_utils import PathUtils


class SupportedArchiveTypes(enum.Enum):
    ZIP = 0
    TAR = 1

    @staticmethod
    def from_str(type_str):
        type_str_lower = type_str.lower()
        if type_str_lower == "zip":
            return SupportedArchiveTypes.ZIP
        elif type_str_lower in ("tar", "tar.gz", "tar.xz", "tar.bz2"):
            return SupportedArchiveTypes.TAR
        else:
            raise ArchiveError(f"Unsupported archive type: {type_str}")


class ArchiveHandlerFactory(object):
    @staticmethod
    def make_from_path(path_to_archive):
        ext_mapping = {
            SupportedArchiveTypes.ZIP: ZipHandler,
            SupportedArchiveTypes.TAR: TarHandler,
        }
        archive_type = SupportedArchiveTypes.from_str(
            PathUtils.get_extension(path_to_archive)
        )
        return ext_mapping[archive_type](path_to_archive=path_to_archive)

    @staticmethod
    def try_to_make(file_obj):
        handlers = {
            ZipHandler,
            TarHandler,
        }
        for handler in handlers:
            try:
                return handler(file_obj=file_obj)
            except ArchiveError:
                pass

        return None
