from logger import logger
from petite_genre.core.session_data import SessionData
from petite_genre.database.db_manager import DbManager
from petite_genre.processing.dim_reduction.abstract_dim_reduction import (
    DimReductionError,
)
from petite_genre.processing.dim_reduction.dim_reduction_method import (
    DimReductionMethod,
)
from petite_genre.processing.dim_reduction.pca_wrapper import PcaWrapper


class DimReductionMethodFactory(object):
    DIM_REDUCTION_CREATORS = {DimReductionMethod.PCA: PcaWrapper}

    def __init__(self, db_manager: DbManager):
        self._db_manager = db_manager

    def make(self, session_data: SessionData):
        try:
            if self._db_manager.trained_reductor_exists_already(
                session_data.session_id
            ):
                return self._make_existing_reductor(session_data)

            logger.debug("Making purely new reductor")
            return DimReductionMethodFactory.DIM_REDUCTION_CREATORS[
                session_data.dim_reduction_method
            ](session_data.result_dims_num)
        except KeyError as e:
            raise DimReductionError(f"Unsupported reduction method: {e}")

    def _make_existing_reductor(self, session_data):
        logger.debug("Loading an existing reductor")
        method_used, model_path, dims_num = self._db_manager.get_dim_reduction(
            session_data.session_id
        )
        logger.debug(
            f"method_used={method_used}, model_path={model_path}, dims_num={dims_num}"
        )
        try:
            reductor = DimReductionMethodFactory.DIM_REDUCTION_CREATORS[method_used](
                dims_num
            )
            reductor.load(model_path)
            return reductor
        except Exception as e:
            raise DimReductionError(
                f"Error while loading saved model: {method_used} - {model_path}"
            ) from e
