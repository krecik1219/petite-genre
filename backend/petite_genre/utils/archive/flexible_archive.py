from petite_genre.utils.archive.handlers.ArchiveHandlerFactory import (
    ArchiveHandlerFactory,
)


class FlexibleArchive(object):
    def __init__(self, path_to_archive):
        self._concrete_handler = ArchiveHandlerFactory.make_from_path(path_to_archive)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()

    def close(self):
        if self._concrete_handler is not None:
            self._concrete_handler.close()

    def extract_all(self, output_dir_path):
        self._concrete_handler.extract_all(output_dir_path)

    @staticmethod
    def is_supported_archive(file_obj):
        with ArchiveHandlerFactory.try_to_make(file_obj) as archive:
            return archive is not None
