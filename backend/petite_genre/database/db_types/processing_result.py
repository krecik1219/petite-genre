import datetime

from mongoengine import (
    DateTimeField,
    Document,
    EmbeddedDocumentListField,
    StringField,
    EmbeddedDocumentField,
)

from petite_genre.constants import MONGO_DB_ALIAS
from petite_genre.utils.identifier import get_uuid
from .dim_reduction import DimReduction
from .prediction import Prediction

from .tracks_data import TracksData


class ProcessingResult(Document):
    uuid = StringField(default=get_uuid, primary_key=True)
    date = DateTimeField(default=datetime.datetime.now)
    tracks_data = EmbeddedDocumentField(TracksData)
    predictions = EmbeddedDocumentListField(Prediction)
    dim_reduction = EmbeddedDocumentField(DimReduction)

    meta = {"db_alias": MONGO_DB_ALIAS, "collection": "processing_results"}
