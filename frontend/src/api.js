import axios from 'axios'

export const api = {
    uploadFile: async (file, email, sessionId) => {
      const formData = new FormData();
      formData.append('file', file);
      formData.append('email', email);
      formData.append('session_id', sessionId);
      return await axios.post('upload', formData, {
        headers: {
          'Content-Type': 'multipart/form-data',
        }
      });
    },
    fetchSession: async (sessionId) => {
      return await axios.get('session', {
        params: {
          'session_id': sessionId
        }
      });
    }
};
