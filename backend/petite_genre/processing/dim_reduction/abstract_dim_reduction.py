from abc import ABC, abstractmethod
from typing import List

from typing_extensions import final

from petite_genre.core import dim_reduction_result as dim_red_res, Prediction
from petite_genre.processing.dim_reduction.dim_reduction_method import (
    DimReductionMethod,
)


class DimReductionError(Exception):
    pass


class AbstractDimReduction(ABC):
    @final
    def reduce_dimensions(
        self, predictions: List[Prediction]
    ) -> "dim_red_res.DimReductionResult":
        return dim_red_res.DimReductionResult(
            method_used=self._get_reduction_method(),
            new_dims_num=self._get_new_dims_num(),
            dim_reduction=self,
            tracks_with_reduced_dim=self._reduce_predictions_dimensions(predictions),
        )

    @abstractmethod
    def _get_reduction_method(self) -> DimReductionMethod:
        pass

    @abstractmethod
    def _get_new_dims_num(self):
        pass

    @abstractmethod
    def _reduce_predictions_dimensions(
        self, predictions: List[Prediction]
    ) -> List["dim_red_res.TrackWithReducedDim"]:
        pass

    @abstractmethod
    def save(self, path):
        pass

    @abstractmethod
    def load(self, path):
        pass
