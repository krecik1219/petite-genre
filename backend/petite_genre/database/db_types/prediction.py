from typing import List, Dict

from mongoengine import (
    EmbeddedDocument,
    ReferenceField,
    StringField,
    ListField,
    FloatField,
)

from petite_genre.database.db_types.track import Track
import petite_genre.core as core


class Prediction(EmbeddedDocument):
    track = ReferenceField(Track)
    predicted_genre = StringField(required=True)
    distribution = ListField()
    before_softmax_activations = ListField(FloatField())

    @staticmethod
    def make(predictions: List[core.Prediction], tracks: Dict[str, Track]):
        return [
            Prediction(
                track=tracks[prediction.track.uuid],
                predicted_genre=prediction.predicted_genre,
                distribution=prediction.distribution,
                before_softmax_activations=prediction.before_softmax_activations.squeeze().tolist(),
            )
            for prediction in predictions
        ]
