from typing import List

from dataclasses import dataclass

import numpy as np

from petite_genre.core.track import Track
from petite_genre.processing.dim_reduction.dim_reduction_method import (
    DimReductionMethod,
)
from petite_genre.processing.dim_reduction import abstract_dim_reduction as abs_dim_red


@dataclass
class TrackWithReducedDim(object):
    track: Track
    dim_reduced_output: np.ndarray


@dataclass
class DimReductionResult(object):
    method_used: DimReductionMethod
    new_dims_num: int
    dim_reduction: "abs_dim_red.AbstractDimReduction"
    tracks_with_reduced_dim: List[TrackWithReducedDim]
