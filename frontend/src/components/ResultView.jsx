import {useState} from 'react'
import {api} from '../api'

const ResultView = (props) => {
  const [sessionId, setSessionId] = useState(props.match.params.session_id || '');
  const [message, setMessage] = useState('');

  const onSessionIdChange = (e) => {
    setSessionId(e.target.value);
  }

  const validateInput = () => {
    if (sessionId === '') {
      setMessage("No session id provided");
      return false;
    }

    return true;
  }

  const onGet = async () => {
    if (validateInput()) {
      try {
        const response = await api.fetchSession(sessionId);
        console.log(response.data);
      } catch (error) {
        console.log(error);
        if (!error.response) {
          setMessage("Unknown error");
        } else if (error.response.status === 500 || error.response.status === 422) {
          setMessage("Internal server error");
        } else if (error.response.status === 404) {
          setMessage("Invalid UUID provided");
        }
      }
    }
  }
  return (
    <div>
      <div>
        <label>Session id: </label>
        <input type="text" value={sessionId} onChange={onSessionIdChange} />
      </div>
      <button onClick={onGet}>Get result</button><p>{message}</p>
    </div>
  );
}

export default ResultView;
