from email_validator import EmailNotValidError, validate_email

from logger import logger
from petite_genre.processing.dim_reduction.dim_reduction_method import (
    DimReductionMethod,
)
from petite_genre.utils.archive.flexible_archive import FlexibleArchive


def email_type(email_str):
    logger.debug("Validating e-mail address.")
    try:
        validated_email = validate_email(email_str)
        logger.debug("E-mail validated.")
        return validated_email["email"]
    except EmailNotValidError as e:
        raise ValueError("Invalid email address format") from e


def archive_type(file):
    logger.debug("Checking archive type.")
    try:
        if FlexibleArchive.is_supported_archive(file.stream._file):
            logger.debug("Archive is valid.")
            file.stream.seek(0)
            return file
    except Exception as e:
        logger.error(f"Error during archive checking: {e}")
    logger.error("Invalid archive")
    raise ValueError("File is not valid archive")


def dim_reduction_method_type(dim_reduction_method_str):
    logger.debug("Checking dimensions reduction method")
    try:
        return DimReductionMethod[dim_reduction_method_str.upper()]
    except Exception as e:
        logger.error(f"Error during dim reduction method conversion: {e}")
    raise ValueError(
        "Provided string is not valid reduction method. Please provide one of: [PCA]"
    )


def positive_int(arg):
    logger.debug("Checking positive int")
    try:
        num = int(arg)
        if num > 0:
            return num
    except ValueError as e:
        logger.error(f"Error during int conversion: {e}")

    raise ValueError(
        f"Provided result dimensions number ({arg}) is not positive integer"
    )
