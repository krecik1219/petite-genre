import os
from typing import List

from mongoengine import Document, StringField

from petite_genre.constants import MONGO_DB_ALIAS
from petite_genre.utils.identifier import get_uuid
import petite_genre.core as core


class Track(Document):
    uuid = StringField(default=get_uuid, primary_key=True)
    track_name = StringField(required=True)
    meta = {"db_alias": MONGO_DB_ALIAS, "collection": "tracks"}

    @staticmethod
    def make(tracks: List[core.Track]):
        return {
            track.uuid: Track(
                uuid=track.uuid, track_name=os.path.basename(track.track_path)
            )
            for track in tracks
        }
