import pathlib

from flask import Flask
from flask_restful import Api

from logger import logger_init, logger
from petite_genre.constants import UPLOAD_DIR
from petite_genre.resources.processing_result import ProcessingResult
from petite_genre.resources.session_record import SessionRecord
from petite_genre.resources.tracks_upload import TracksUpload
from petite_genre.utils.app_config_validation import validate_startup_config


def create_app():
    logger_init("app")
    validate_startup_config()
    logger.info("Starting petite-genre backend")
    app = Flask(__name__)
    api = Api(app)

    api.add_resource(TracksUpload, "/upload")
    api.add_resource(ProcessingResult, "/result")
    api.add_resource(SessionRecord, "/session")
    pathlib.Path(UPLOAD_DIR).mkdir(exist_ok=True)

    return app
