from mongoengine import EmbeddedDocument, ReferenceField, FloatField, ListField

from petite_genre.database.db_types.track import Track


class TrackDimReduced(EmbeddedDocument):
    track = ReferenceField(Track)
    reduced_dim = ListField(FloatField())
