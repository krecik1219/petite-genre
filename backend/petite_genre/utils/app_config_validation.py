from petite_genre.constants import MAIL_CREDENTIALS


def validate_startup_config():
    if MAIL_CREDENTIALS["address"] is None or MAIL_CREDENTIALS["password"] is None:
        raise RuntimeError(
            "Please set environment variables 'MAIL_ADDRESS' and 'MAIL_PASSWORD' prior to running petite-genre server"
        )
