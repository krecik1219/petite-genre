from dataclasses import dataclass

from petite_genre.core import TracksData
from petite_genre.processing.dim_reduction.dim_reduction_method import (
    DimReductionMethod,
)


@dataclass
class SessionData(object):
    session_id: str
    tracks_data: TracksData
    dim_reduction_method: DimReductionMethod
    result_dims_num: int

    def __post_init__(self):
        # needed due to imperfection of serialization of celery's tasks' arguments
        # nested dataclasses are stored in a deserialized object as dicts (tracks_data in this case)
        # enums are stored as an underlying type instead of the enum type
        if isinstance(self.tracks_data, dict):
            self.tracks_data = TracksData(**self.tracks_data)
        self.dim_reduction_method = DimReductionMethod(self.dim_reduction_method)
