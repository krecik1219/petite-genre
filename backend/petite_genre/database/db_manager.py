import petite_genre.database.db_types as db
from petite_genre.database.DbError import DbError
from petite_genre.processing.dim_reduction.dim_reduction_method import (
    DimReductionMethod,
)
from petite_genre.processing.tracks_processing_chain import TracksProcessingResult
import petite_genre.core as core


class DbManager(object):
    def store_tracks_processing_result(
        self,
        session_data: core.SessionData,
        tracks_processing_result: TracksProcessingResult,
    ):
        tracks_data = session_data.tracks_data
        tracks = db.Track.make(tracks_processing_result.tracks)
        tracks_data_db = db.TracksData.make(tracks_data, tracks.values())
        predictions = db.Prediction.make(tracks_processing_result.predictions, tracks)
        dim_reduction = db.DimReduction.make(
            tracks_data.base_dir, tracks_processing_result.dim_reduction_result, tracks
        )
        processing_result = db.ProcessingResult(
            uuid=tracks_data.uuid,
            tracks_data=tracks_data_db,
            predictions=predictions,
            dim_reduction=dim_reduction,
        )
        self._save_tracks(tracks.values())
        self._save_processing_result(processing_result)
        self._save_session_record(session_data.session_id, processing_result)
        return processing_result

    def _save_tracks(self, tracks):
        for track in tracks:
            track.save()

    def _save_processing_result(self, processing_result):
        processing_result.save()

    def _save_session_record(self, session_id, processing_result):
        session_record = self.get_session_record(session_id)
        if session_record is None:
            session_record = db.SessionRecord(
                session_id=session_id,
                training_result=processing_result,
                additional_results=[],
            )
            session_record.save()
        elif session_record.training_result is None:
            session_record.update(set__training_result=processing_result)
        else:
            session_record.update(push__additional_results=processing_result)

    def get_session_record(self, session_id):
        return db.SessionRecord.objects(session_id=session_id).first()

    def trained_reductor_exists_already(self, session_id):
        session_record = self.get_session_record(session_id)
        if session_record is None:
            return False

        return session_record.training_result is not None

    def get_dim_reduction(self, session_id):
        session_record = self.get_session_record(session_id)
        if session_record is None:
            raise DbError(
                f"Session record for given session_id: {session_id} does not exist"
            )

        method_used = DimReductionMethod[
            session_record.training_result.dim_reduction.method_used
        ]
        reduction_model_path = (
            session_record.training_result.dim_reduction.reduction_model_path
        )
        new_dims_num = session_record.training_result.dim_reduction.new_dims_num
        return method_used, reduction_model_path, new_dims_num
