from dataclasses import asdict
from flask_restful import Resource, reqparse

from logger import logger
from petite_genre.constants import (
    HTTP_SUCCESS_RESPONSE_CODE,
    HTTP_UNPROCESSABLE_ENTITY_CODE,
    HTTP_INTERNAL_SERVER_ERROR_CODE,
    HTTP_NOT_FOUND_CODE,
)
from petite_genre.processing.dim_reduction.dim_reduction_method import (
    DimReductionMethod,
    DEFAULT_RESULT_DIMS_NUM,
)

from petite_genre.utils.identifier import get_uuid
from petite_genre.core.session_data import SessionData
from petite_genre.core.tracks_data import save_tracks_data_with_uuid
from petite_genre.utils.args_types import (
    archive_type,
    email_type,
    dim_reduction_method_type,
    positive_int,
)
from petite_genre.tasks.process_tracks import process_tracks_data
from petite_genre.utils.validation.validators import is_valid_session_id


class TracksUpload(Resource):
    parser = reqparse.RequestParser(bundle_errors=True)
    parser.add_argument("file", type=archive_type, location="files", required=True)
    parser.add_argument("email", type=email_type, required=True)
    parser.add_argument(
        "dim_red_method", type=dim_reduction_method_type, required=False
    )
    parser.add_argument("result_dims_num", type=positive_int, required=False)
    parser.add_argument("session_id", type=str, required=False)

    def post(self):
        logger.info("Received request")
        data = self.parser.parse_args(http_error_code=HTTP_UNPROCESSABLE_ENTITY_CODE)
        logger.debug(f"Requests data: {data}")
        session_id = data["session_id"]
        if session_id and not is_valid_session_id(session_id):
            return {
                "message": f"Session record with given session_id: {data['session_id']} does not exist"
            }, HTTP_NOT_FOUND_CODE

        try:
            tracks_archive = data["file"]
            tracks_data = save_tracks_data_with_uuid(tracks_archive, get_uuid())
            tracks_data.email = data["email"]
            dim_reduction_method = data["dim_red_method"] or DimReductionMethod.PCA
            result_dims_num = data["result_dims_num"] or DEFAULT_RESULT_DIMS_NUM
            session_id = data["session_id"] or get_uuid()
            session_data = SessionData(
                session_id, tracks_data, dim_reduction_method, result_dims_num
            )
            process_tracks_data.apply_async((asdict(session_data),))
            return {
                "message": "tracks uploaded",
                "session_id": session_id,
                "proc_result_uuid": tracks_data.uuid,
            }, HTTP_SUCCESS_RESPONSE_CODE
        except Exception as e:
            logger.error(f"Error during processing result req: {e}")
            return {"message": "Internal server error"}, HTTP_INTERNAL_SERVER_ERROR_CODE
