import React, {useState, useCallback} from 'react'
import {useDropzone} from 'react-dropzone'
import {api} from '../api'
import Message from './Message'

function DropZone() {
  const [email, setEmail] = useState('');
  const [sessionId, setSessionId] = useState('');
  const [selectedFile, setSelectedFile] = useState(null);
  const [message, setMessage] = useState('');
  const onDrop = useCallback(acceptedFile => {
      setSelectedFile(acceptedFile[0]);
  }, []);
  const {getRootProps, getInputProps} = useDropzone({onDrop, multiple: false})

  const onEmailChange = e => {
    setEmail(e.target.value);
  };

  const onSessionIdChange = e => {
    setSessionId(e.target.value);
  };

  const validateInput = () => {
    if (selectedFile === null) {
      setMessage("No file selected");
      return false;
    } else if (email === "") {
      setMessage("No email provided");
      return false;
    }

    return true;
  }

  const onSend = async () => {
    if (validateInput()) {
      try {
        setMessage('');
        console.log(selectedFile);
        const response = await api.uploadFile(selectedFile, email, sessionId);
        console.log(response);
        if (response.status === 200) {
          setMessage("Analysis has started");
        }
      } catch (error) {
        console.log(error);
        if (!error.response) {
          setMessage("Unknown error");
        } else if (error.response.status === 500) {
          setMessage("Internal server error");
        } else if (error.response.status === 422) {
          setMessage("Invalid data provided");
        }
      }
    }
  }

  return (
    <div
    style={{
        position: 'absolute', left: '50%', top: '50%',
        transform: 'translate(-50%, -50%)',
        height: "400px"
    }}
    >
      <div {...getRootProps()} style={{textAlign: "center", lineHeight: "100px", height: "100px", width: "100%", border: "2px dashed #f69c55", marginBottom: "20px"}}>
      <input {...getInputProps()} />
      {
        <p>Drop file here or click to select one</p>
      }
      </div>
      <div>
        <label style={{width: "90px"}}>E-mail: </label>
        <input type="email" onChange={onEmailChange} />
        <p class="text-muted" style={{fontSize: "11px"}}>You will receive e-mail when processing is done</p>
      </div>
      <div style={{marginBottom: "10px"}}>
        <label style={{width: "90px"}}>Session id: </label>
        <input onChange={onSessionIdChange} />
        <p class="text-muted" style={{fontSize: "11px"}}>Fill session id if result should be appended to existing ones</p>
      </div>
      <button onClick={onSend}>Send</button>
      <div style={{ marginTop: 10 }}>
        {message ? <Message msg={message} /> : null}
      </div>
    </div>
  );
}

const FileUploadView = () => {
    return (
        <div>
          <DropZone />
        </div>
    );
};

export default FileUploadView;
