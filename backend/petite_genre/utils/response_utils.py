import gzip

from bson import json_util
import json

from flask import make_response


def make_compressed_response(result_json, result_code):
    json_str = json.dumps(result_json, default=json_util.default).encode("utf-8")
    json_compressed = gzip.compress(json_str)
    response = make_response(json_compressed)
    response.status_code = result_code
    response.mimetype = "application/json"
    response.headers["Content-length"] = len(json_compressed)
    response.headers["Content-Encoding"] = "gzip"
    return response
