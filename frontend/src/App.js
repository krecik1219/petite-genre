import FileUploadView from './components/FileUploadView'
import ResultView from './components/ResultView'
import TopBar from './components/TopBar'
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from 'react-router-dom';

function App() {
  return (
    <div className="App">
      <TopBar />
        <Router>
          <Switch>
            <Route exact path="/" component={FileUploadView}/>
            <Route path="/result/:session_id?" component={ResultView}/>
          </Switch>
        </Router>
    </div>
  );
}

export default App;
