from petite_genre.core.session_data import SessionData
from petite_genre.processing.dim_reduction.dim_reduction_method_factory import (
    DimReductionMethodFactory,
)
from petite_genre.tasks.celeryapp import celery

from logger import logger
from petite_genre.database.db_manager import DbManager
from petite_genre.processing.tracks_processing_chain import TracksProcessingChain
from petite_genre.utils.mailing_service import (
    send_analysis_result,
    send_failed_analysis_result,
)


class ProcessTracksDataImpl(object):
    def __init__(self, session_data_dict):
        self._session_data = SessionData(**session_data_dict)
        self._tracks_data = self._session_data.tracks_data
        self._db_manager = DbManager()
        self._dim_red_factory = DimReductionMethodFactory(self._db_manager)

    def run(self):
        try:
            tracks_processing_chain = TracksProcessingChain(
                self._tracks_data, self._dim_red_factory.make(self._session_data)
            )
            processing_chain_result = tracks_processing_chain.process_training_dataset()
            if processing_chain_result.successful:
                return self._store_to_db_and_send_mail_with_result(
                    processing_chain_result
                )
            else:
                logger.warning(
                    f"Processing unsuccessful. TracksData={self._tracks_data},"
                    f" error_msg: {processing_chain_result.err_msg}"
                )
                self._send_email(False, None, None, self._tracks_data.email)
                return None
        except Exception as e:
            logger.error(
                f"Exception caught at the top level of the process_tracks_data task. Error: {e}"
            )
            self._send_email(False, None, None, self._tracks_data.email)
            return None

    def _store_to_db_and_send_mail_with_result(self, processing_chain_result):
        processing_result = self._db_manager.store_tracks_processing_result(
            self._session_data, processing_chain_result
        )
        logger.info(
            f"Processing successful. session_id={self._session_data.session_id}, uuid={processing_result.uuid}"
        )
        self._send_email(
            True,
            self._session_data.session_id,
            processing_result.uuid,
            self._session_data.tracks_data.email,
        )
        return processing_result.uuid

    def _send_email(self, success, session_id, uuid, mail_addr):
        try:
            if success:
                send_analysis_result(session_id, uuid, mail_addr)
            else:
                send_failed_analysis_result(session_id, mail_addr)
        except Exception as e:
            logger.error(
                f"Error while sending an email. session_id={session_id}, uuid={uuid}, mail_addr={mail_addr}. Error: {e}"
            )


@celery.task
def process_tracks_data(session_data_dict):
    return ProcessTracksDataImpl(session_data_dict).run()
