class PathUtils(object):
    @staticmethod
    def get_extension(path):
        path = str(path)
        index = path.find(".")
        if index == -1:
            return ""
        else:
            return path[(index + 1) :]
