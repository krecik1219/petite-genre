import os
from typing import List

from dataclasses import dataclass

from logger import logger
from petite_genre.constants import DATASET_BASE_DIR_NAME
from petite_genre.core.track import Track
from petite_genre.core.tracks_data import TracksData
from petite_genre.processing.dim_reduction.abstract_dim_reduction import (
    DimReductionError,
    AbstractDimReduction,
)
from petite_genre.core.dim_reduction_result import DimReductionResult
from petite_genre.processing.predicting.genres_predictor import GenresPredictor
from petite_genre.core.prediction import Prediction
from petite_genre.utils.archive.archive_error import ArchiveError
from petite_genre.utils.archive.flexible_archive import FlexibleArchive
from petite_genre.utils.identifier import get_uuid


@dataclass
class TracksProcessingResult(object):
    uuid: str = ""
    successful: bool = False
    err_msg: str = ""
    tracks: List[Track] = None
    predictions: List[Prediction] = None
    dim_reduction_result: DimReductionResult = None


class TracksProcessingChain(object):
    def __init__(self, tracks_data: TracksData, reductor: AbstractDimReduction):
        self._tracks_data = tracks_data
        self._reductor = reductor

    def process_training_dataset(self) -> TracksProcessingResult:
        try:
            self._extract_archive()
            tracks = self._get_potential_tracks()
            predictions = self._recognize_tracks_genres(tracks)
            if len(predictions) == 0:
                return TracksProcessingResult(
                    successful=False, err_msg="Predictions are empty"
                )

            dim_reduction_result = self._dimensionality_reduction(predictions)
            return TracksProcessingResult(
                uuid=self._tracks_data.uuid,
                successful=True,
                tracks=tracks,
                predictions=predictions,
                dim_reduction_result=dim_reduction_result,
            )

        except (ArchiveError, DimReductionError) as e:
            logger.error(f"Exception during dataset processing: {e}")
            return TracksProcessingResult(successful=False, err_msg=str(e))

    def _extract_archive(self):
        dirpath = os.path.dirname(self._tracks_data.archive_path)
        output_path = os.path.join(dirpath, DATASET_BASE_DIR_NAME)
        with FlexibleArchive(self._tracks_data.archive_path) as archive:
            archive.extract_all(output_path)

        self._tracks_data.unpacked_path = output_path

    def _get_potential_tracks(self):
        filenames = os.listdir(self._tracks_data.unpacked_path)
        filepaths = [
            os.path.join(self._tracks_data.unpacked_path, filename)
            for filename in filenames
        ]
        potential_tracks = [
            Track(uuid=get_uuid(), track_path=filepath)
            for filepath in filepaths
            if os.path.isfile(filepath)
        ]
        return potential_tracks

    def _recognize_tracks_genres(self, tracks):
        genres_predictor = GenresPredictor(tracks)
        genres_predictor.predict_all()
        return genres_predictor.predictions

    def _dimensionality_reduction(self, predictions):
        return self._reductor.reduce_dimensions(predictions)
