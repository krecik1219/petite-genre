import os


BASE_DIR = os.path.dirname(os.path.realpath(__file__))

LOGS_DIR = os.path.dirname(os.path.join(BASE_DIR, "logs/"))
UPLOAD_DIR = os.path.join(BASE_DIR, "uploads/")
DATASET_BASE_DIR_NAME = "songs/"
HTTP_SUCCESS_RESPONSE_CODE = 200
HTTP_NOT_FOUND_CODE = 404
HTTP_UNPROCESSABLE_ENTITY_CODE = 422
HTTP_INTERNAL_SERVER_ERROR_CODE = 500
HTTP_NOT_FOUND = 404

MONGO_DB_ALIAS = "results"
MONGO_DB_NAME = "petite_genre"
MONGO_DB_HOST = os.getenv("MONGO_DB_HOST") or "localhost"
mongo_port = os.getenv("MONGO_DB_PORT")
MONGO_DB_PORT = int(mongo_port) if mongo_port else 27017

REDIS_CACHE_URL = os.getenv("REDIS_CACHE_URL") or "redis://localhost:6379"
REDIS_KEY_PREFIX = "petite-genre"

CELERY_BROKER_URL = os.getenv("CELERY_BROKER_URL") or "redis://localhost:6379"
CELERY_RESULT_BACKEND = os.getenv("CELERY_RESULT_BACKEND") or "redis://localhost:6379"


WEBSITE_URL = os.getenv("WEBSITE_URL") or "localhost:3000"
MAIL_CREDENTIALS = {
    "address": os.getenv("MAIL_ADDRESS") or None,
    "password": os.getenv("MAIL_PASSWORD") or None,
}
