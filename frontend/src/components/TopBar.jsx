import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';

const TopBar = (props) => {
    return (
        <Navbar
            className="navbar-color"
            collapseOnSelect
            expand="lg"
            variant="dark"
            bg="dark"
        >
          <Navbar.Brand href="/">Petite Genre</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="mr-auto">
              <Nav.Link href="/">File upload</Nav.Link>
              <Nav.Link href="/result">Result view</Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
    );
}

export default TopBar;
