from dataclasses import dataclass


@dataclass(eq=True, frozen=True)
class Track(object):
    uuid: str
    track_path: str
