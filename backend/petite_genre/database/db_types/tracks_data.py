from typing import List

from mongoengine import StringField, ListField, ReferenceField, EmbeddedDocument

from petite_genre.database.db_types.track import Track
import petite_genre.core as core


class TracksData(EmbeddedDocument):
    base_dir = StringField(required=True)
    archive_path = StringField(required=True)
    unpacked_path = StringField(required=True)
    tracks = ListField(ReferenceField(Track))
    email = StringField(required=True)

    @staticmethod
    def make(tracks_data: core.TracksData, tracks: List[core.Track]):
        return TracksData(
            base_dir=tracks_data.base_dir,
            archive_path=tracks_data.archive_path,
            unpacked_path=tracks_data.unpacked_path,
            tracks=tracks,
            email=tracks_data.email,
        )
