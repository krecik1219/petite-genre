import tarfile

from petite_genre.utils.archive.archive_error import ArchiveError
from petite_genre.utils.archive.handlers.archive_handler import ArchiveHandler


class TarHandler(ArchiveHandler):
    def __init__(self, path_to_archive=None, file_obj=None):
        super().__init__(path_to_archive=path_to_archive, file_obj=file_obj)
        self._tar_file_handle: tarfile.TarFile = None
        if self._path_to_archive is None and self._file_obj is None:
            raise ArchiveError(
                "Invalid archive args. Expected path to archive or file like object"
            )
        try:
            if self._path_to_archive is not None:
                self._tar_file_handle = tarfile.open(self._path_to_archive)
            elif self._file_obj is not None:
                self._tar_file_handle = tarfile.open(fileobj=self._file_obj)
        except Exception as e:
            raise ArchiveError(f"Cannot open tar archive. Error: {e}") from e

    def close(self):
        if self._tar_file_handle is not None:
            self._tar_file_handle.close()

    def extract_all(self, output_dir_path):
        try:
            self._tar_file_handle.extractall(output_dir_path)
        except Exception as e:
            raise ArchiveError(
                f"Could not extract archive: {self._path_to_archive}. Error: {e}"
            ) from e
