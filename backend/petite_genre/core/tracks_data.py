import os
from pathlib import Path

from dataclasses import dataclass
from werkzeug.utils import secure_filename

from logger import logger
from petite_genre.constants import UPLOAD_DIR


@dataclass
class TracksData(object):
    uuid: str
    base_dir: str
    archive_path: str
    unpacked_path: str = None
    email: str = None


def save_tracks_data_with_uuid(tracks_data_archive, request_uuid):
    logger.debug("Saving file with UUID.")
    request_unique_dir = os.path.join(UPLOAD_DIR, request_uuid)
    Path(request_unique_dir).mkdir()
    filepath = os.path.join(
        request_unique_dir, secure_filename(tracks_data_archive.filename)
    )
    tracks_data_archive.save(filepath)
    os.chmod(request_unique_dir, 0o777)
    os.chmod(filepath, 0o777)
    logger.info(f"Saved file with UUID: {request_uuid}, path: {filepath}")
    return TracksData(
        uuid=request_uuid, base_dir=request_unique_dir, archive_path=filepath
    )
