from petite_genre.database.db_manager import DbManager


def is_valid_session_id(session_id):
    return DbManager().get_session_record(session_id) is not None
